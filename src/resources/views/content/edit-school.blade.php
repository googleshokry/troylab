@extends('layout/master')

@section('menu')
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="dropdown nav-item" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/students">
                    <i data-feather="edit"></i>Students
                </a>
            </li>
            <li class="dropdown nav-item active" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/schools">
                    <i data-feather="edit"></i>Schools
                </a>
            </li>

        </ul>
    </div>
@endsection

@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Form</h4>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        {!! Form::model($school, ['method' => 'PATCH', 'action' => ['\App\Http\Controllers\WEB\V1\SchoolController@update',$school->id]]) !!}
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="first-name">Name</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="first-name" class="form-control" name="name"
                                               placeholder="Name" value="{{$school->name}}">
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-9 offset-sm-3">
                                <button type="submit"
                                        class="btn btn-primary me-1 waves-effect waves-float waves-light">Submit
                                </button>
                            </div>
                        </div>
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
