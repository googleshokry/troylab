@extends('layout/master')

@section('menu')
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="dropdown nav-item" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/students">
                    <i data-feather="edit"></i>Students
                </a>
            </li>
            <li class="dropdown nav-item" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/schools">
                    <i data-feather="edit"></i>Schools
                </a>
            </li>

        </ul>
    </div>
@endsection
@section('content')


@endsection
