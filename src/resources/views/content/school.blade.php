@extends('layout/master')

@section('menu')
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="dropdown nav-item" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/students">
                    <i data-feather="edit"></i>Students
                </a>
            </li>
            <li class="dropdown nav-item active" data-menu="dropdown">
                <a class=" nav-link d-flex align-items-center" href="/schools">
                    <i data-feather="edit"></i>Schools
                </a>
            </li>

        </ul>
    </div>
@endsection

@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                <div class="card-header border-bottom p-1">
                    <div class="dt-action-buttons text-end">
                        <div class="dt-buttons d-inline-flex">

                            <a href="/schools/create" class="create-new btn btn-primary" tabindex="0"
                               aria-controls="DataTables_Table_0"
                               data-bs-target="#modals-slide-in"><span><svg xmlns="http://www.w3.org/2000/svg"
                                                                            width="24" height="24"
                                                                            viewBox="0 0 24 24" fill="none"
                                                                            stroke="currentColor"
                                                                            stroke-width="2"
                                                                            stroke-linecap="round"
                                                                            stroke-linejoin="round"
                                                                            class="feather feather-plus me-50 font-small-4"><line
                                            x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19"
                                                                                        y2="12"></line></svg>Add New Record</span>
                            </a>
                        </div>
                    </div>
                </div>
                <table class="table dataTable no-footer dtr-column collapsed"
                       id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info"
                       style="width: 1443px;">
                    <thead>
                    <tr role="row">

                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                            style="width: 339px;" aria-label="Name: activate to sort column ascending">Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                            style="width: 97px;" aria-label="Salary: activate to sort column ascending">Created At
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                            style="width: 123px; "
                            aria-label="Status: activate to sort column ascending">Updated At
                        </th>
                        <th class="" rowspan="1" colspan="1" style="width: 92px;"
                            aria-label="Actions">Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($schools as $school)
                        <tr class="even">
                            <td>
                                <div class="d-flex justify-content-left align-items-center">
                                    <div class="avatar  me-1"><img src="../../../app-assets/images/avatars/10-small.png"
                                                                   alt="Avatar" width="32" height="32"></div>
                                    <div class="d-flex flex-column"><span
                                            class="emp_name text-truncate fw-bold">{{ $school->name }}</span></div>
                                </div>
                            </td>
                            <td>{{ $school->created_at}}</td>
                            <td>{{ $school->updated_at}}</td>
                            <td>
                                <a href="/schools/{{$school->id}}" class="item-edit">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-edit font-small-4">
                                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                    </svg>
                                </a>
                                {!! Form::open (['method'=>'DELETE', 'action'=>['\App\Http\Controllers\WEB\V1\SchoolController@destroy', $school->id]]) !!}
                                <button type="submit" class="btn btn-danger">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                         stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-trash-2 font-small-4 me-50">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path
                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        <line x1="10" y1="11" x2="10" y2="17"></line>
                                        <line x1="14" y1="11" x2="14" y2="17"></line>
                                    </svg>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br/>
                <br/>
                <br/>
                <div class="d-flex justify-content-between mx-0 row">
                    <div class="col-sm-12">
                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                            {{ $schools->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
