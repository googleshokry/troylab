<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'status', 'school_id', 'order'];

    function school()
    {
        return $this->belongsTo(School::class);
    }
}
