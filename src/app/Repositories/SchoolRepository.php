<?php

namespace App\Repositories;


use App\Models\School as Model;

class SchoolRepository
{
    private $model;

    public function __construct(Model $model = null)
    {
        $this->model = $model ?? new Model();
    }

    public function query()
    {
        return $this->model::query();
    }

    public function on($connection)
    {
        return $this->model::on($connection);
    }

    public function findBy($where)
    {
        return $this->model::where($where)->first();
    }

    public function get()
    {
        return $this->model::all();
    }

    public function paginate()
    {
        return $this->model::paginate();
    }

    public function store($request)
    {
        $this->model->name = $request->name;
        $this->model->save();
        return $this->model;
    }

    public function update($id, $request)
    {
        $this->model = $this->model::find($id);
        $this->model->name = $request->name;
        $this->model->update();
        return $this->model;
    }

    public function destroy($id)
    {
        return $this->model::find($id)->delete();
    }
}
