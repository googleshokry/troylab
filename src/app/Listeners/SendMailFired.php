<?php

namespace App\Listeners;

use App\Events\SendMail;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendMail $event
     * @return void
     */
    public function handle(SendMail $event)
    {
        $user = User::find($event->userId)->toArray();
//        dd($user);
//        Mail::to('prasanna@enfection.com')->send(new sendMail($errorData));

        Mail::send('emails.mailEvent', $user, function ($message) use ($user) {
            $message->to($user['email']);
            $message->subject('Event Testing');
        });
    }
}
