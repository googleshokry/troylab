<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class Repositories
{
    static function search($query)
    {
        try {
            $para = '?sort=stars&order=desc';
            if (isset($query['numberOfItems']))
                $para .= '&per_page=' . $query['numberOfItems'];
            else
                $para .= '&per_page=10';
            if (isset($query['page']))
                $para .= '&page=' . $query['page'];
            else
                $para .= '&page=1';
            if (isset($query['date']))
                $para .= '&q=created:' . $query['date'];
            else
                $para .= '&q=created:>' . Carbon::yesterday()->format('Y-m-d');
            $response = Http::get('https://api.github.com/search/repositories?' . $para);
            $collection = array_map(function ($data) {
                $newdata['id'] = $data['id'];
                $newdata['full_name'] = $data['full_name'];
                $newdata['html_url'] = $data['html_url'];
                $newdata['created_at'] = Carbon::parse($data['created_at'])->format('Y-m-d');
                $newdata['updated_at'] = Carbon::parse($data['updated_at'])->format('Y-m-d');
                return $newdata;
            }, $response->json()['items']??[]);
            $collection = array_merge($response->json(), ['items' => $collection]);
            return $collection;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
