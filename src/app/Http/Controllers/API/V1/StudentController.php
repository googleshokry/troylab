<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\APIController;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends APIController
{
    public function index()
    {
        $responses = Student::all();
        return $this->setLog(false)->setContent($responses)->setResult(true)->send();
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "school_id" => "required"
        ]);

        $responses = Student::create($request->all());
        return $this->setLog(false)->setContent($responses)->setResult(true)->send();

    }

    public function show(Student $student)
    {
        return $this->setLog(false)->setContent($student)->setResult(true)->send();

    }

    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $student->update($request->all());

        return $this->setLog(false)->setContent($student)->setResult(true)->send();

    }

    public function destroy($id)
    {
        $student = Student::find($id);
        if ($student) {
            $student->delete();
            return $this->setLog(false)->setContent(['student deleted successfully'])->setResult(true)->send();
        } else
            return $this->setLog(false)->setContent(["can't delete student"])->setResult(true)->send();
    }

}
