<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\APIController;
use App\Models\School;
use App\Repositories\SchoolRepository as Repository;
use Illuminate\Http\Request;

class SchoolController extends APIController
{
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $responses = Repository::get();
        return $this->setLog(false)->setContent($responses)->setResult(true)->send();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $responses = Repository::create($request->all());
        return $this->setLog(false)->setContent($responses)->setResult(true)->send();

    }

    public function show(Repository $school)
    {
        return $this->setLog(false)->setContent($school)->setResult(true)->send();

    }

    public function update(Request $request, Repository $school)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $school->update($request->all());
        return $this->setLog(false)->setContent($school)->setResult(true)->send();

    }

    public function destroy($id)
    {
        $school = Repository::destroy($id);
        if ($school)
            return $this->setLog(false)->setContent(['School deleted successfully'])->setResult(true)->send();
        else
            return $this->setLog(false)->setContent(["can't delete School"])->setResult(true)->send();
    }

}
