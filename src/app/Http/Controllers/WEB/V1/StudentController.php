<?php

namespace App\Http\Controllers\WEB\V1;

use App\Repositories\SchoolRepository;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class StudentController extends Controller
{
    private $repository;

    public function __construct(StudentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $students = $this->repository->paginate();
        return view('content.student', ['students' => $students]);

    }

    public function edit($id)
    {
        $student = $this->repository->findBy(["id" => $id]);
        $schools = (new SchoolRepository())->get();
        return view('content.edit-student', ['student' => $student, "schools" => $schools]);
    }

    public function create()
    {

        $schools = (new SchoolRepository())->get();
        return view('content.create-student', ["schools" => $schools]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "name" => "required",
            "school_id" => "required",
            "order" => "required"
        ]);
        if ($validator->fails()) {
            return redirect('students/create')
                ->withErrors($validator)
                ->withInput();
        }
        $this->repository->store($request);
        return redirect()->back()->with('message', 'Added successfully!');

    }

    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "name" => "required",
            "school_id" => "required",
            "order" => "required"
        ]);
        if ($validator->fails()) {
            return redirect('students/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $this->repository->update($id, $request);

        return redirect()->back()->with('message', 'Update completed successfully!');
    }

    public function destroy($id)
    {
        $this->repository->destroy($id);
        return redirect()->back()->with('message', 'Update completed successfully!');

    }

}
