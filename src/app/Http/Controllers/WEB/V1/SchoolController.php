<?php

namespace App\Http\Controllers\WEB\V1;

use App\Http\Controllers\API\APIController;
use App\Repositories\SchoolRepository as Repository;
use Illuminate\Http\Request;

class SchoolController extends APIController
{

    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $schools = $this->repository->paginate();
        return view('content.school', ['schools' => $schools]);

    }

    public function edit($id)
    {
        $school = $this->repository->findBy(["id" => $id]);
        return view('content.edit-school', ['school' => $school]);
    }

    public function create()
    {
        return view('content.create-school');
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "name" => "required"
        ]);
        if ($validator->fails()) {
            return redirect('schools/create')
                ->withErrors($validator)
                ->withInput();
        }
        $this->repository->store($request);
        return redirect()->back()->with('message', 'Added successfully!');

    }

    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "name" => "required",
        ]);
        if ($validator->fails()) {
            return redirect('schools/' . $id)
                ->withErrors($validator)
                ->withInput();
        }

        $this->repository->update($id, $request);

        return redirect()->back()->with('message', 'Update completed successfully!');
    }

    public function destroy($id)
    {
        $this->repository->destroy($id);
        return redirect()->back()->with('message', 'Delete completed successfully!');

    }
}
