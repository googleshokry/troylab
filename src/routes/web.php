<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("/", function () {
    return view("content.index");
});

Route::get("/students/create", "\App\Http\Controllers\WEB\V1\StudentController@create")->name('create-student');
Route::get("/students", "\App\Http\Controllers\WEB\V1\StudentController@index");
Route::post("/students", "\App\Http\Controllers\WEB\V1\StudentController@store")->name('store-student');
Route::get("/students/{id}", "\App\Http\Controllers\WEB\V1\StudentController@edit")->name('edit-student');
Route::delete("/students/{id}", "\App\Http\Controllers\WEB\V1\StudentController@destroy")->name('delete-student');
Route::patch("/students/update/{id}", "\App\Http\Controllers\WEB\V1\StudentController@update")->name('update-student');



Route::get("/schools/create", "\App\Http\Controllers\WEB\V1\SchoolController@create")->name('create-school');
Route::get("/schools", "\App\Http\Controllers\WEB\V1\SchoolController@index");
Route::post("/schools", "\App\Http\Controllers\WEB\V1\SchoolController@store")->name('store-school');
Route::get("/schools/{id}", "\App\Http\Controllers\WEB\V1\SchoolController@edit")->name('edit-school');
Route::delete("/schools/{id}", "\App\Http\Controllers\WEB\V1\SchoolController@destroy")->name('delete-school');
Route::patch("/schools/update/{id}", "\App\Http\Controllers\WEB\V1\SchoolController@update")->name('update-school');
