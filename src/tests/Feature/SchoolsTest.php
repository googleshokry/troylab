<?php

namespace Tests\Feature;

use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class SchoolsTest extends TestCase
{

    public function testJson()
    {
        // json get All Student
        $response = $this->get('/api/v1/school');
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );



        // json Add school
        $testData = [
            'name' => 'school name'
        ];
        $response = $this->post('/api/v1/school',$testData);
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );

        $school_id=$response->json()['content']['data']['id'];

        // json Edit Student
        $testData = [
            'name' => 'school2'
        ];
        $response = $this->put('/api/v1/school/'.$school_id,$testData,['Accept'=>'application/json']);
        $response
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );

        // json Show school
        $response = $this->get('/api/v1/school/'.$school_id,['Accept'=>'application/json']);
        $response
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );

        // json Delete school
        $response = $this->delete('/api/v1/school/'.$school_id);
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );



    }
}
