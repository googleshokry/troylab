<?php

namespace Tests\Feature;

use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class StudentsTest extends TestCase
{

    public function testJson()
    {
        // json get All Student
        $response = $this->get('/api/v1/student');
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );



        // json Add Student
        $testData = [
            'name' => 'shokry',
            'school_id'=>1
        ];
        $response = $this->post('/api/v1/student',$testData);
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );
        $student_id=$response->json()['content']['data']['id'];

        // json Edit Student
        $testData = [
            'name' => 'shokry2',
            'school_id'=>2
        ];
        $response = $this->put('/api/v1/student/'.$student_id,$testData,['Accept'=>'application/json']);
        $response
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );

        // json Show Student
        $response = $this->get('/api/v1/student/'.$student_id,['Accept'=>'application/json']);
        $response
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );

        // json Delete student
        $response = $this->delete('/api/v1/student/'.$student_id);
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );



    }
}
