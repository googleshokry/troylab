## Usage

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this, and spin up the containers for the web server by running `docker-compose up -d --build site`.

After that completes, run this code

- `docker-compose run --rm composer update`
- `docker-compose run --rm artisan migrate`
- `docker-compose run --rm artisan db:seed`
- `docker-compose run artisan student:prefix`

Note:- 
The following are built for our web server, with their exposed ports detailed:

- **nginx** - `:80`
- **php** - `:9000`
- **mysql** - `:3306`


You can use By

API By

PostmanCollection File 

for Testing run 

``
- `docker-compose run artisan test`
``
